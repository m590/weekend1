const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const Artist = mongoose.model("Artist");

module.exports.getAllAlbums = function (req, res) {
    const artistId = req.params.artistId;
    Artist.findById(artistId).exec(function (err, artist) {
        const response = {
            status: 200,
            content: artist.albums
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        res.status(response.status).json(response.content);
    });
};

module.exports.getOneAlbum = function (req, res) {
    const artistId = req.params.artistId;
    const albumId = req.params.albumId;

    Artist.findById(artistId).select('albums').exec(function (err, artist) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        if (response.status == 200) {
            console.log('artist', artist);
            response.content = artist.albums.id(albumId);
        }
        res.status(response.status).json(response.content);
    });
};

const _saveAlbum = function (req, res, artist) {
    artist.albums.push({
        title: req.body.title,
        releasedYear: parseInt(req.body.releasedYear),
        genre: req.body.genre,
        producer: req.body.producer
    });

    artist.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error get games', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.addAlbum = function (req, res) {
    const artistId = req.params.artistId;

    Artist.findById(artistId).exec(function (err, artist) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        if (response.status == 200) {
            _saveAlbum(req, res, artist);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};

const _albumUpdate = function (req, res, artist) {
    const albumId = req.params.albumId;
    for (let i = 0; i < artist.albums.length; i++) {
        if (artist.albums[i]._id == albumId) {
            artist.albums[i].title = req.body.title;
            artist.albums[i].releasedYear = parseInt(req.body.releasedYear);
            artist.albums[i].genre = req.body.genre;
            artist.albums[i].producer = req.body.producer;
            break;
        }
    }

    artist.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error update album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.updateAlbum = function (req, res) {
    const artistId = req.params.artistId;

    Artist.findById(artistId).exec(function (err, artist) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        if (response.status == 200) {
            _albumUpdate(req, res, artist);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};

const _albumPartialUpdate = function (req, res, artist) {
    const albumId = req.params.albumId;
    for (let i = 0; i < artist.albums.length; i++) {
        if (artist.albums[i]._id == albumId) {
            if (req.body.title) {
                artist.albums[i].title = req.body.title;
            }
            if (req.body.releasedYear) {
                artist.albums[i].releasedYear = parseInt(req.body.releasedYear);
            }
            if (req.body.genre) {
                artist.albums[i].genre = req.body.genre;
            }
            if (req.body.producer) {
                artist.albums[i].producer = req.body.producer;
            }
            break;
        }
    }

    artist.save(function (err, savedStudent) {
        const response = {
            status: 201,
            content: savedStudent
        };

        if (err) {
            console.log('Error update album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        }
        res.status(response.status).json(response.content);
    });
}

module.exports.updatePartialAlbum = function (req, res) {
    const artistId = req.params.artistId;

    Artist.findById(artistId).exec(function (err, artist) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        if (response.status == 200) {
            _albumPartialUpdate(req, res, artist);
        } else {
            res.status(response.status).json(response.content);
        }
    });
};

module.exports.deleteAlbum = function (req, res) {
    const artistId = req.params.artistId;
    const albumId = req.params.albumId;

    const findQry = {
        _id: ObjectId(artistId),
    }
    const updateQry = {
        $pull: {albums: {_id: ObjectId(albumId)}}
    }

    Artist.updateOne(findQry, updateQry, {}, function (err, artist) {
        const response = {
            status: 200
        };

        if (err) {
            console.log('Error get album', err);
            response.status = 500;
            response.content = {content: 'Internal error'};
        } else if (!artist) {
            response.status = 400;
            response.content = {content: 'Artist not found'};
        }
        res.status(response.status).json(response.content);
    });
};