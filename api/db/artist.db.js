const mongoose = require('mongoose');
require('./artist.model');

const url = process.env.db_url + process.env.db_name;

console.log('url', url);

mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true, useCreateIndex: true}, function () {
    console.log('artist db connected');
});
