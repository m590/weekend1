const config = require('dotenv').config();
const express = require('express');
require('./api/db/artist.db');
const router = require('./api/router/router');

const app = express();
app.use(express.json());
app.use("/api", router);


const server = app.listen(process.env.port, function () {
    console.log('Artists server started at', server.address().port);
});